package janstheo.stts.com.tugas_pertama

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast

class MainActivity : AppCompatActivity(), LoginFragment.InputActionListener {

    private var password_asli = "stts"
    private lateinit var recentFrag: RecentFragment


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buatsupport1()
        buatsupport2()
    }

    private fun buatsupport2() {
        recentFrag = RecentFragment.newInstance()
        supportFragmentManager.beginTransaction().apply {
            replace(
                    R.id.frame2,
                    recentFrag
            )
            commit()
        }
    }

    private fun buatsupport1() {
        val fragment = LoginFragment.newInstance(this)
        val begin1 = supportFragmentManager.beginTransaction()
        begin1.replace(
                R.id.frame1, fragment
        )
        begin1.commit()
    }

    override fun getLogin(us: String, pw: String) {
        Toast.makeText(
                this,
                us,
                Toast.LENGTH_SHORT
        ).show()
        if (us.isEmpty() || pw.isEmpty()) {
            recentFrag.cekLogin("Username dan Password tidak boleh kosong!")
        } else if (pw.equals(password_asli)) {
            openAnotherActivity(us)
        } else {
            recentFrag.cekLogin("Password harus 'stts'")
        }
    }

    private fun openAnotherActivity (us: String) {
        val intent = SecondActivity.getStartIntent(this, us)
        startActivity(intent)
    }

}
