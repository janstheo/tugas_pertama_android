package janstheo.stts.com.tugas_pertama


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : Fragment() {

    private lateinit var listener: InputActionListener

    companion object {
        fun newInstance(listener: InputActionListener) : LoginFragment {
            val fragment = LoginFragment()
            fragment.listener = listener
            return fragment
        }
    }

    interface InputActionListener{
        fun getLogin(us: String, pw: String)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
                R.layout.fragment_login,
                container,
                false
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btn_login.setOnClickListener{
            var usn = username.text.toString()
            var pwd = password.text.toString()
            listener.getLogin(usn, pwd)
        }
    }

}